package crypto

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"strings"
	"time"
)

type PEM struct {
	Host        string
	ValidFrom   string
	ValidFor    time.Duration
	IsCA        bool
	RsaBits     int
	EcdsaCurve  string
	IsGenerated bool
	Template    x509.Certificate
	Priv        interface{}
}

//NewPEM creates new PEM
/*

	host       = flag.String("host", "", "Comma-separated hostnames and IPs to generate a certificate for")
	validFrom  = flag.String("start-date", "", "Creation date formatted as Jan 1 15:04:05 2011")
	validFor   = flag.Duration("duration", 365*24*time.Hour, "Duration that certificate is valid for")
	isCA       = flag.Bool("ca", false, "whether this cert should be its own Certificate Authority")
	rsaBits    = flag.Int("rsa-bits", 2048, "Size of RSA key to generate. Ignored if --ecdsa-curve is set")
	ecdsaCurve = flag.String("ecdsa-curve", "", "ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521")

	PEM{
		Host:        *host,
		ValidFrom:   *validFrom,
		ValidFor:    *validFor,
		IsCA:        false,
		RsaBits:     *rsaBits,
		EcdsaCurve:  *ecdsaCurve,
		IsGenerated: false,
		Template:    x509.Certificate{},
	}

	err := p.Generate("cert.pem", "key.pem")
	if err != nil {
		panic(err)
	}
*/
func NewPEM(host string) *PEM {
	return &PEM{
		Host:        host,
		ValidFrom:   "",
		ValidFor:    365 * 24 * time.Hour,
		IsCA:        false,
		RsaBits:     2048,
		EcdsaCurve:  "",
		IsGenerated: false,
		Template:    x509.Certificate{},
	}
}

func (p *PEM) Create() error {

	var (
		err                 error
		notAfter, notBefore time.Time
	)

	if len(p.Host) == 0 {
		return errors.New("Missing required --host parameter")
	}

	if p.IsGenerated {
		return nil
	}

	switch p.EcdsaCurve {
	case "":
		p.Priv, err = rsa.GenerateKey(rand.Reader, p.RsaBits)
	case "P224":
		p.Priv, err = ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	case "P256":
		p.Priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	case "P384":
		p.Priv, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	case "P521":
		p.Priv, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	default:
		fmt.Fprintf(os.Stderr, "Unrecognized elliptic curve: %q", p.EcdsaCurve)
		os.Exit(1)
	}
	if err != nil {
		log.Fatalf("failed to generate private key: %s", err)
	}

	if len(p.ValidFrom) == 0 {
		notBefore = time.Now()
	} else {
		notBefore, err = time.Parse("Jan 2 15:04:05 2006", p.ValidFrom)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to parse creation date: %s\n", err)
			os.Exit(1)
		}
	}

	notAfter = notBefore.Add(p.ValidFor)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	p.Template = x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Acme Co"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	hosts := strings.Split(p.Host, ",")
	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			p.Template.IPAddresses = append(p.Template.IPAddresses, ip)
		} else {
			p.Template.DNSNames = append(p.Template.DNSNames, h)
		}
	}

	if p.IsCA {
		p.Template.IsCA = true
		p.Template.KeyUsage |= x509.KeyUsageCertSign
	}

	p.IsGenerated = true
	return nil
}

func (p *PEM) Generate(cert, key string) error {

	if !p.IsGenerated {
		err := p.Create()
		if err != nil {
			return err
		}
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &p.Template, &p.Template, publicKey(p.Priv), p.Priv)
	if err != nil {
		return err
	}

	certOut, err := os.Create(cert)
	if err != nil {
		return err
	}

	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		log.Fatalf("failed to write data to %s: %s", cert, err)
	}
	if err := certOut.Close(); err != nil {
		return err
	}
	log.Printf("wrote %s \n", cert)

	keyOut, err := os.OpenFile(key, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Printf("failed to open %s for writing: %s", key, err)
		return err
	}
	if err := pem.Encode(keyOut, pemBlockForKey(p.Priv)); err != nil {
		log.Fatalf("failed to write data to key.pem: %s", err)
	}
	if err := keyOut.Close(); err != nil {
		log.Fatalf("error closing key.pem: %s", err)
	}
	log.Printf("wrote %s\n", key)
	return nil

}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
			os.Exit(2)
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}
