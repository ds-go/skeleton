package skeleton

import (
	"flag"
	"time"

	overlay "gitlab.com/ds-go/overlay"
)

var (
	//Template overlay.Overlay
	Template overlay.Overlay
	//WaitFlag graceful shutdown delay
	WaitFlag time.Duration
	//WriteTimeoutFlag HTTP time.duration
	WriteTimeoutFlag time.Duration
	//ReadTimeoutFlag HTTP time.duration
	ReadTimeoutFlag time.Duration
	//IdleTimeoutFlag HTTP time.duration
	IdleTimeoutFlag time.Duration
	//DevMode disable browser cache and enabled debugging pprof
	DevMode bool
)

//InitFlags create skeleton flags
func InitFlags() {
	flag.DurationVar(
		&WaitFlag,
		"graceful-timeout",
		time.Second*3,
		"the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m",
	)

	flag.DurationVar(
		&WriteTimeoutFlag,
		"writetimeout",
		time.Second*30,
		"the writeTimeout duration for http connections",
	)

	flag.DurationVar(
		&ReadTimeoutFlag,
		"readtimeout",
		time.Second*30,
		"the writeTimeout duration for http connections",
	)

	flag.DurationVar(
		&IdleTimeoutFlag,
		"idletimeout",
		time.Second*60,
		"the writeTimeout duration for http connections",
	)

	flag.BoolVar(
		&DevMode,
		"dev",
		true,
		"enable dev mode: disable cache with verbose loggin",
	)

}
