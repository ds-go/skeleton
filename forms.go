package skeleton

import (
	"encoding/json"
	"net/http"
	"regexp"
)

//GetInputs returns all r.FormValues from http request
//missing inputs are returned as empty strings
func GetInputs(r *http.Request, input ...string) (map[string]string, error) {
	inputs := make(map[string]string)
	for _, v := range input {
		inputs[v] = r.FormValue(v)
	}
	return inputs, nil
}

//Recaptcha configuration
type Recaptcha struct {
	token    string
	url      string
	secret   string
	remoteip string
}

//RecaptchaResponse challenge response from Recaptcha servers
type RecaptchaResponse struct {
	Success bool `json:"success"`
}

//NewRecaptcha returns instance of Recaptcha
func NewRecaptcha(token, url, secret string) *Recaptcha {
	return &Recaptcha{
		token:    token,
		url:      url,
		secret:   secret,
		remoteip: "127.0.0.1",
	}
}

//Verify Recaptcha challenge
func (r *Recaptcha) Verify(input string) bool {

	res := &RecaptchaResponse{false}

	ipres, err := http.Get(r.url)
	if err != nil {
		return false
	}
	r.remoteip = ipres.Request.RemoteAddr
	ipres.Body.Close()

	resp, err := http.Get(r.url + "?secret=" + r.secret + "&response=" + r.token + "&remoteip=" + r.remoteip)
	if err != nil {
		return false
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(res)

	if err != nil {
		return false
	}

	return res.Success
}

//VerifyEmail pattern match
func VerifyEmail(email string) bool {
	pattern := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if pattern.MatchString(email) {
		return true
	}
	return false
}
