package skeleton

import (
	"encoding/json"
	"log"
	"net/http"
	"runtime/debug"
	"strings"

	dispatcher "gitlab.com/ds-go/dispatcher"
)

//ResponseInterface for responding to requests
type ResponseInterface interface {
	Respond(w http.ResponseWriter)
}

//Response JSON data struct
type Response struct {
	Code  int
	Data  string
	Error string
}

//Render page from template
func Render(w http.ResponseWriter, layout string, data interface{}) error {
	return Template.Render(w, layout, data)
}

//RenderTagged page from template
func RenderTagged(w http.ResponseWriter, r *http.Request, layout string, tag string, data interface{}) error {
	if DevMode == false {
		if dispatcher.ETag(layout+tag, 30, w, r) {
			return nil
		}
	}
	return Render(w, layout, data)
}

//RenderStatic Render Cached static page render
func RenderStatic(w http.ResponseWriter, r *http.Request, layout string, tag string) error {
	return RenderTagged(w, r, layout, tag, nil)
}

//DecodeJSON decodes json response to interface
func DecodeJSON(r *http.Request, v interface{}) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&v)
	if err != nil {
		log.Println(err)
	}
	return err
}

//NewResponse returns simple Response example
func NewResponse() *Response {
	return &Response{
		Code:  http.StatusInternalServerError,
		Data:  "",
		Error: "Internal Server Error",
	}
}

//Respond writes data to http.ResponseWriter
func (r *Response) Respond(w http.ResponseWriter) {

	js, err := json.Marshal(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.Code)
	w.Write(js)
}

func debugMultipleResponse(p []byte) {
	if strings.Contains(string(p), "multiple response.WriteHeader") {
		debug.PrintStack()
	}
}
