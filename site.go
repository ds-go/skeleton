package skeleton

import (
	"net/http"

	dispatcher "gitlab.com/ds-go/dispatcher"
	overlay "gitlab.com/ds-go/overlay"
)

//Site interface website extraction from skeleton
type Site interface {
	//Load template files
	Load() overlay.Overlay
	//Get Dispatch routes
	Dispatcher() dispatcher.Dispatcher
	//NewHTTPServer
	NewHTTPServer() *http.Server
}

//LoadTemplate overlay.Overlay
func LoadTemplate(t overlay.Overlay) {
	Template = t
}
